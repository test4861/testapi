﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using testApi.Models;

namespace testApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DialogController : ControllerBase
    {
        private readonly ILogger<DialogController> _logger;

        /// <summary>
        /// Dialog
        /// </summary>
        public DialogController(ILogger<DialogController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Поиск диалога по Guid клиентам
        /// </summary>
        /// <remarks>
        /// Sample request:
        /// 
        ///     POST api/Dialog/SearchDialog
        ///     [
        ///         "F35D7AFB-C0EF-40BC-4593-08D95329CC0B",
        ///         "B1521CDC-28A3-48F6-E0DD-08D952518E1B",
        ///     ]
        /// </remarks>
        [HttpPost(nameof(SearchDialog))]
        public Guid SearchDialog(IEnumerable<Guid> guidClients)
        {
            try
            {
                var dialogsClients = new RGDialogsClients().Init();

                return dialogsClients
                    .AsParallel()
                    .GroupBy(x => x.IDRGDialog)
                    .Select(x => new
                    {
                        dialogId = x.Key,
                        clients = x.Select(s => s.IDClient).ToList()
                    })
                    .Where(x => guidClients.All(s => x.clients.Any(a => a.Equals(s))))
                    .Select(x => x.dialogId)
                    .FirstOrDefault();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return Guid.Empty;
            }
            
        }
    }
}
